#ifndef __STRING_CAS__

	#define	__STRING_CAS__

	#include <Windows.h>

	#define			BYTE_2_DEVIDER				(DWORD_PTR	)(sizeof(__int16	) - 1)
	#define			BYTE_4_DEVIDER				(DWORD_PTR	)(sizeof(__int32	) - 1)
	#define			BYTE_8_DEVIDER				(DWORD_PTR	)(sizeof(__int64	) - 1)

	void			mbs_to_wcs_cas				(char* mbs, wchar_t* wcs								);

	char			strcmp_cas					(char* First, char* Second								);
	unsigned int	strlen_cas					(char* Str												);
	void			strcpy_cas					(char* Dest, char* Source								);
	unsigned int	replace_name_cas			(char* inSource, unsigned int inMaxLen, char* inName	);
	char*			get_module_file_name_cas	(HMODULE hModule, HANDLE processHeap					);

	unsigned int	wcslen_cas					(wchar_t* Str											);
	unsigned int	wcs_byte_len_cas			(wchar_t* Str											);
	void			wcscpy_cas					(wchar_t* Dest, wchar_t* Source							);

	//***		mem-functions

	void			memcpy_cas					(void* Dest, void* Source, DWORD_PTR Size				);

#endif