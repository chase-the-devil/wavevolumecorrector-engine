#include <Windows.h>
#include "String_cas.h"

#define		HANDLE_OFFSET(inHandle, inOffset, inType)\
				((inType*)((DWORD_PTR)(inHandle) + inOffset))

#define		LI_FULL(inLargeInteger)\
				(inLargeInteger).QuadPart

#define		HLI_FULL(inLargeInteger)\
				(inLargeInteger)->QuadPart

#define		BIT_RATE_8				(__int16			)(8						)
#define		BIT_RATE_16				(__int16			)(16					)
#define		BIT_RATE_24				(__int16			)(24					)
#define		BIT_RATE_32				(__int16			)(32					)

#define		TOP_16_BIT_INT			(__int16			)(0x7fff				)
#define		TOP_24_BIT_INT			(__int32			)(0x007fffff			)
#define		TOP_32_BIT_INT			(__int32			)(0x7fffffff			)
#define		TOP_64_BIT_INT			(__int64			)(0x7fffffffffffffff	)

#define		TOP_32_BIT_FLOAT		(float				)(3.402823466e+38		)

#define		WAVE_FORMAT_UNKNOWN		(unsigned __int16	)(0x0000				)
#define		WAVE_FORMAT_IEEE_FLOAT	(unsigned __int16	)(0x0003				)

//in string format its "."
#define		NODE_CURR_FOLDER_TAG	(__int16			)(0x002E				)

//in string format its ".."
#define		NODE_PREV_FOLDER_TAG	(__int32			)(0x00002E2E			)


//in string format its L"."
#define		NODE_CURR_FOLDER_TAGW	(__int32			)(0x0000002E			)

//in string format its L".."
#define		NODE_FOLDERS_TAGW		(__int64			)(0x00000000002E002E	)

//in string format its "WAVE"
#define		FORMAT_WAVE				(unsigned __int32	)(0x45564157			)

//in string format its "data"
#define		DATA_SECTION			(unsigned __int32	)(0x61746164			)

//in string format its "RIFF"
#define		RIFF_SECTION			(unsigned __int32	)(0x46464952			)

//in string format its "fmt "
#define		FMT_SECTION				(unsigned __int32	)(0x20746d66			)

//in string format its ".wav"
#define		WAV_EXTENSION			(unsigned __int32	)(0x7661772e			)
#define		WAV_EXTENSION_LENGTH	(unsigned __int32	)(0x00000004			)

typedef
	#pragma pack(1)
	struct ChunkIdentifiers
		{
			__int8						chunkId		[4];
			unsigned __int32			chunkSize;
		}

	sChunkIdentifiers,
	*hsChunkIdentifiers;

typedef
	#pragma pack(1)
	struct ChunkExtraParameters
		{
			unsigned __int16			extraParamSize;
			void*						extraParams;
		}

	sChunkExtraParameters,
	*hsChunkExtraParameters;

typedef
	struct EXTRASubChunk
		{		
			#pragma pack(1)
			struct
				{
					sChunkIdentifiers	identifiers;
					void*				subchunkData;
				}	
			
			data;
			
			struct EXTRASubChunk*		nextChunk;
		}

	sEXTRASubChunk,
	*hsEXTRASubChunk;

typedef
	#pragma pack(1)
	struct EXTRASubChunksSection
		{
			unsigned __int32			subchunksSize;
			unsigned __int32			subchunksCount;
			hsEXTRASubChunk				extraSubchunks;
		}

	sEXTRASubChunksSection,
	*hsEXTRASubChunksSection;

typedef
	#pragma pack(1)
	struct RIFFHeader
		{
			sChunkIdentifiers			identifiers;
			__int8						format		[4];
		}

	sRIFFHeader,
	*hsRIFFHeader;

typedef
	#pragma pack(1)
	struct RIFFChunk
		{
			sRIFFHeader					riff;
			// Probably can't be extra info in riff chunk, 
			// but gonna leave it there for future, 
			// who knows what they'll do xD
			//sChunkExtraParameters		riffExtra;
		}
	
	sRIFFChunk,
	*hsRIFFChunk;

typedef
	#pragma pack(1)
	struct FMTDataPart
		{
			/*	
				PCM = 1 (i.e. Linear quantization)
				Values other than 1 indicate some 
				form of compression.		
			*/
			unsigned __int16			audioFormat;

			//	Mono = 1, Stereo = 2, etc.
			unsigned __int16			numChannels;

			//	8000, 44100, etc.
			unsigned __int32			sampleRate;

			//	== SampleRate * NumChannels * BitsPerSample/8
			unsigned __int32			byteRate;

			/*
				== NumChannels * BitsPerSample/8
				The number of bytes for one sample including
				all channels. I wonder what happens when
				this number isn't an integer?
			*/
			unsigned __int16			blockAlign;

			//	8 bits = 8, 16 bits = 16, etc.
			unsigned __int16			bitsPerSample;
		}

	sFMTDataPart;

typedef
	#pragma pack(1)
	struct FMTHeader
		{
			sChunkIdentifiers			identifiers;
			sFMTDataPart				dataPart;
		}

	sFMTHeader,
	*hsFMTHeader;

typedef
	#pragma pack(1)
	struct FMTChunk
		{
			sFMTHeader					fmt;
			sChunkExtraParameters		fmtExtra;
		}

	sFMTChunk;

typedef
	#pragma pack(1)
	struct WAVHeader
		{
			sRIFFChunk					chunkRiff;
			sEXTRASubChunksSection		postRiff;
			sFMTChunk					chunkFmt;
			sEXTRASubChunksSection		postFmt;
			sChunkIdentifiers			data;
			LARGE_INTEGER				postExtraSize;
		}

	sWAVHeader, 
	*hsWAVHeader;

typedef
	struct WAVImage
		{
			hsWAVHeader					header;
			void*						data;
			void*						postExtraData;
		}

	sWAVImage,
	*hsWAVImage;

typedef
	void __stdcall ProgressCallback(double);

typedef
	ProgressCallback *hProgressCallback;

typedef
	void __stdcall FinalCallback();

typedef
	FinalCallback *hFinalCallback;

typedef
	struct WorkerParam
		{
			char*						paths;
			char*						copyPath;
			double						value;
			hProgressCallback			callback;
			hFinalCallback				finalCallback;
		}

	sWorkerParam,
	*hsWorkerParam;

//***		globals

HANDLE		procHeap;

//***		prototypes

						DWORD				__stdcall	Worker						(hsWorkerParam inParam														);

						void							create_directories			(wchar_t* nodePath															);
						void							process_wav_folder_replace	(
																						char* inPath, 
																						double inVolume, 
																						hProgressCallback callback, 
																						LARGE_INTEGER allSize, 
																						PLARGE_INTEGER currSize
																					);
						void							process_wav_folder_copy		(
																						char* inPath, 
																						char* copyPath, 
																						char* inReserved, 
																						double inVolume, 
																						hProgressCallback callback, 
																						LARGE_INTEGER allSize, 
																						PLARGE_INTEGER currSize
																					);
						unsigned __int16				change_volume_of_image		(
																						hsWAVImage inImage, 
																						double inCoef, 
																						hProgressCallback callback, 
																						LARGE_INTEGER allSize, 
																						PLARGE_INTEGER currSize	
																					);
						LARGE_INTEGER					get_folder_size				(char* inPath																);

						hsWAVHeader						read_wav_header_simple		(HANDLE inFile																);
						void							write_wav_header_simple		(HANDLE inFile, hsWAVHeader inHeader										);
						void							release_wav_header_simple	(hsWAVHeader inHeader														);

						hsWAVImage						read_wav_image_simple		(HANDLE inFile																);
						void							write_wav_image_simple		(HANDLE inFile, hsWAVImage inImage											);
						void							release_wav_image_simple	(hsWAVImage inImage															);

//***		interface

__declspec(dllexport)	void				__stdcall	ProcessFoldersAsync			(
																						char* paths, 
																						char* copyPath, 
																						double value, 
																						hProgressCallback callback, 
																						hFinalCallback finalCallback	
																					);