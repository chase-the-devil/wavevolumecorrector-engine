#include "String_cas.h"

void mbs_to_wcs_cas(char* mbs, wchar_t* wcs)
	{
		while(*mbs)
			{
				*wcs = *mbs;

				++mbs;
				++wcs;
			}

		*wcs = *mbs;
	}

char strcmp_cas(char* First, char* Second)
	{
		while(*First && !(*First ^ *Second))
			{
				++First;
				++Second;
			}

		return *First | *Second;
	}

unsigned int strlen_cas(char* Str)
	{
		const char* Base = Str;

		while(*Str)
			++Str;

		return (unsigned int)(Str - Base);
	}

void strcpy_cas(char* Dest, char* Source)
	{
		while(*Source)
			{
				*Dest = *Source;

				++Dest;
				++Source;
			}

		*Dest = *Source;
	}

unsigned int replace_name_cas(char* inSource, unsigned int inMaxLen, char* inName)
	{
		const char* Base			= inSource;
		char*		LastSeparator	= inSource;

		while(*inSource)
			{
				if(*inSource == '\\')
					{
						inMaxLen -= (unsigned int)(inSource - LastSeparator + 1);
						LastSeparator = inSource;
					}

				++inSource;
			}

		inSource = LastSeparator + 1;
		++inMaxLen;

		while(inMaxLen && *inName)
			{
				*inSource = *inName;

				--inMaxLen;
				++inName;
				++inSource;
			}

		return (unsigned int)(inSource - Base);
	}

char* get_module_file_name_cas(HMODULE hModule, HANDLE processHeap)
	{
		size_t	size = 64;
		char*	buffer;
		DWORD	ret;
	
	__loop:
			buffer	= (char*)HeapAlloc(processHeap, 0, size + 1);
			ret		= GetModuleFileNameA(hModule, buffer, size);
			
			if(ret < size && ret != 0)
				return buffer;

			if(GetLastError() == ERROR_INSUFFICIENT_BUFFER) 
				{
					HeapFree(processHeap, 0, buffer);
					size >>= 1;
				} 
			else 
				return NULL;

		goto __loop;
	}

unsigned int wcs_byte_len_cas(wchar_t* Str)
	{
		const wchar_t* Base = Str;

		while(*Str)
			++Str;

		return (unsigned int)((DWORD_PTR)(Str) - (DWORD_PTR)(Base));
	}

unsigned int wcslen_cas(wchar_t* Str)
	{
		const wchar_t* Base = Str;

		while(*Str)
			++Str;

		return (unsigned int)(Str - Base) >> 1;
	}

void wcscpy_cas(wchar_t* Dest, wchar_t* Source)
	{
		while(*Source)
			{
				*Dest = *Source;

				++Dest;
				++Source;
			}

		*Dest = *Source;
	}

//***		mem-functions

void memcpy_cas_v1(void* Dest, void* Source, DWORD_PTR Size)
	{
		const DWORD_PTR final = (DWORD_PTR)Dest + Size;

		if(Size >= sizeof(unsigned __int64))
			{
				

				return;
			}

		if(Size >= sizeof(unsigned __int32))
			{
				return;
			}

		if(Size >= sizeof(unsigned __int16))
			{
				return;
			}


	}

void memcpy_cas(void* Dest, void* Source, DWORD_PTR Size)
	{
		const DWORD_PTR final = (DWORD_PTR)Dest + Size;

		if(!(Size & BYTE_8_DEVIDER))
			{
				//8 bytes align
				while(Dest < final)
					{
						*((unsigned __int64*)(Dest)) = *((unsigned __int64*)(Source));

						++((unsigned __int64*)(Dest		));
						++((unsigned __int64*)(Source	));
					}

				return;
			}

		if(!(Size & BYTE_4_DEVIDER))
			{	
				//4 bytes align

				if(Size > sizeof(unsigned __int64))
					{
						//chunk can be processed as 8 bytes align
						DWORD_PTR diff = final - sizeof(unsigned __int32);

						while(Dest < diff)
							{
								*((unsigned __int64*)(Dest)) = *((unsigned __int64*)(Source));

								++((unsigned __int64*)(Dest		));
								++((unsigned __int64*)(Source	));
							}

						//last part of chunk as 4 bytes align
						*((unsigned __int32*)(Dest)) = *((unsigned __int32*)(Source));

						return;
					}
				
				while(Dest < final)
					{
						*((unsigned __int32*)(Dest)) = *((unsigned __int32*)(Source));

						++((unsigned __int32*)(Dest		));
						++((unsigned __int32*)(Source	));
					}

				return;
			}

		if(!(Size & BYTE_2_DEVIDER))
			{
				if(Size > sizeof(unsigned __int64))
					{
						//chunk can be processed as 8 bytes align
						DWORD_PTR diff = final - sizeof(unsigned __int16);

						while(Dest < diff)
							{
								*((unsigned __int64*)(Dest)) = *((unsigned __int64*)(Source));

								++((unsigned __int64*)(Dest		));
								++((unsigned __int64*)(Source	));
							}

						//last part of chunk as 2 bytes align
						*((unsigned __int16*)(Dest)) = *((unsigned __int16*)(Source));

						return;
					}

				if(Size > sizeof(unsigned __int32))
					{
						//chunk can be processed as 8 bytes align
						DWORD_PTR diff = final - sizeof(unsigned __int16);

						while(Dest < diff)
							{
								*((unsigned __int32*)(Dest)) = *((unsigned __int32*)(Source));

								++((unsigned __int32*)(Dest		));
								++((unsigned __int32*)(Source	));
							}

						//last part of chunk as 4 bytes align
						*((unsigned __int16*)(Dest)) = *((unsigned __int16*)(Source));

						return;
					}

				//2 byte align
				while(Dest < final)
					{
						*((unsigned __int16*)(Dest)) = *((unsigned __int16*)(Source));

						++((unsigned __int16*)(Dest		));
						++((unsigned __int16*)(Source	));
					}

				return;
			}

		if(Size > sizeof(unsigned __int64))
			{
				//chunk can be processed as 8 bytes align
				DWORD_PTR diff = final - sizeof(unsigned __int8);

				while(Dest < diff)
					{
						*((unsigned __int64*)(Dest)) = *((unsigned __int64*)(Source));

						++((unsigned __int64*)(Dest		));
						++((unsigned __int64*)(Source	));
					}

				//last part of chunk as 2 bytes align
				*((unsigned __int16*)(Dest)) = *((unsigned __int16*)(Source));

				return;
			}

		if(Size > sizeof(unsigned __int32))
			{
				//chunk can be processed as 8 bytes align
				DWORD_PTR diff = final - sizeof(unsigned __int16);

				while(Dest < diff)
					{
						*((unsigned __int32*)(Dest)) = *((unsigned __int32*)(Source));

						++((unsigned __int32*)(Dest		));
						++((unsigned __int32*)(Source	));
					}

				//last part of chunk as 4 bytes align
				*((unsigned __int16*)(Dest)) = *((unsigned __int16*)(Source));

				return;
			}

		//1 byte align
		while(Dest < final)
			{
				*((unsigned __int8*)(Dest)) = *((unsigned __int8*)(Source));

				++((unsigned __int8*)(Dest		));
				++((unsigned __int8*)(Source	));
			}
	}