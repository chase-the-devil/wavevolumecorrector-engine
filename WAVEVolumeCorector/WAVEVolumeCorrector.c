#include "WAVEVolumeCorrector.h"

BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
	{
		return TRUE;
	}

void __stdcall ProcessFoldersAsync(char* paths, char* copyPath, double value, hProgressCallback callback, hFinalCallback finalCallback)
	{
		procHeap					= GetProcessHeap();

		hsWorkerParam	param		= HeapAlloc(procHeap, 0, sizeof(sWorkerParam));

		char*			pathsBase	= paths;

	__find_the_end:

		while(*pathsBase)
			++pathsBase;

		if(*(++pathsBase))
			goto __find_the_end;

		DWORD_PTR		byteLength	= pathsBase - paths + 1;

		param->paths				= HeapAlloc(procHeap, 0, byteLength);
		memcpy_cas(param->paths, paths, byteLength);

		if(copyPath)
			{
				param->copyPath	= HeapAlloc(procHeap, 0, strlen_cas(copyPath) + 1);
				strcpy_cas(param->copyPath, copyPath);
			}
		else
			param->copyPath		= NULL;

		param->value			= value;
		param->callback			= callback;
		param->finalCallback	= finalCallback;

		CreateThread(NULL, 0, Worker, param, 0, NULL);
	}


DWORD __stdcall Worker(hsWorkerParam inParam)
	{
		unsigned int	pathsLen		= strlen_cas(inParam->paths		);
		unsigned int	copyPathLen;

		if(inParam->copyPath)
			copyPathLen = strlen_cas(inParam->copyPath);
		else
			copyPathLen = 0;

		char*			curr			= inParam->paths;
		LARGE_INTEGER	all_size		= {0};
		HANDLE			currFile;
		hsWAVImage		image;
		hsWAVHeader		header;
		LARGE_INTEGER	currSize		= {0};
		unsigned int	nameLen;

	__parseSize:
		{}

		nameLen							= strlen_cas(curr);

		if(curr[nameLen - 1] == '*')
			LI_FULL(all_size) += LI_FULL(get_folder_size(curr));
		else
			{
				currFile				= CreateFileA	(
															curr,
															GENERIC_READ,
															0,
															NULL,
															OPEN_EXISTING,
															FILE_ATTRIBUTE_NORMAL,
															NULL
														);

				if(header = read_wav_header_simple(currFile))
					{
						LI_FULL(all_size) += header->data.chunkSize;

						release_wav_header_simple(header);
					}

				CloseHandle(currFile);
			}

		while(*curr)
			++curr;

		if(!*(++curr))
			goto __endParseSize;

		goto __parseSize;

	__endParseSize:

		curr = inParam->paths;

		if(!inParam->copyPath)
			{
			__parseReplace:
				
				if(curr[strlen_cas(curr) - 1] == '*')
					process_wav_folder_replace(curr, inParam->value, inParam->callback, all_size, &currSize);
				else
					{
						currFile		= CreateFileA	(
															curr, 
															GENERIC_READ |
															GENERIC_WRITE,
															0,
															NULL,
															OPEN_EXISTING,
															FILE_ATTRIBUTE_NORMAL,
															NULL
														);

						if(image = read_wav_image_simple(currFile))
							{
								change_volume_of_image(image, inParam->value, inParam->callback, all_size, &currSize);

								SetFilePointer(currFile, 0, 0, FILE_BEGIN);

								write_wav_image_simple(currFile, image);
								release_wav_image_simple(image);
							}

						CloseHandle(currFile);
					}

				while(*curr)
					++curr;

				if(!*(++curr))
					goto __endParse;

				goto __parseReplace;
			}
		else
			{
			__parseCopy:

				if(curr[strlen_cas(curr) - 1] == '*')
					process_wav_folder_copy(curr, inParam->copyPath, NULL, inParam->value, inParam->callback, all_size, &currSize);
				else
					{
						currFile		= CreateFileA	(
															curr, 
															GENERIC_READ,
															0,
															NULL,
															OPEN_EXISTING,
															FILE_ATTRIBUTE_NORMAL,
															NULL
														);

						if(image = read_wav_image_simple(currFile))
							{
								change_volume_of_image(image, inParam->value, inParam->callback, all_size, &currSize);

								CloseHandle(currFile);

								char* nameBase		= NULL;
								char* copiedPath	= curr;

								while(*copiedPath)
									{
										if(*copiedPath == '\\')
											{
												nameBase = ++copiedPath;
												continue;
											}

										++copiedPath;
									}

								copiedPath			= HeapAlloc(procHeap, 0, copyPathLen + strlen_cas(nameBase) + 1);
										
								strcpy_cas(copiedPath,					inParam->copyPath	);
								strcpy_cas(&copiedPath[copyPathLen],	nameBase			);

								currFile				= CreateFileA	(
																			copiedPath, 
																			GENERIC_WRITE,
																			0,
																			NULL,
																			CREATE_ALWAYS,
																			FILE_ATTRIBUTE_NORMAL,
																			NULL
																		);

								write_wav_image_simple(currFile, image);

								release_wav_image_simple(image);
								HeapFree(procHeap, 0, copiedPath);
							}

						CloseHandle(currFile);
					}

				while(*curr)
					++curr;

				if(!*(++curr))
					goto __endParse;

				goto __parseCopy;
			}

	__endParse:

		if(inParam->copyPath)
			HeapFree(procHeap, 0, inParam->copyPath	);

		HeapFree(procHeap, 0, inParam->paths	);
		HeapFree(procHeap, 0, inParam			);

		inParam->finalCallback();

		return 0;
	}

LARGE_INTEGER get_folder_size(char* inPath)
	{
		WIN32_FIND_DATAA	data;
		HANDLE				currFile;
		LARGE_INTEGER		size		= {0};
		LARGE_INTEGER		bufSize;
		unsigned int		inPathLen	= strlen_cas(inPath);

		HANDLE				waveFile;
		hsWAVHeader			header;

		currFile						= FindFirstFileA(inPath, &data);

		do
			{
				if(data.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT	)
					goto __continue;

				else if(data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						if	(
								!*data.cFileName									||
								(
									*((__int16*)(data.cFileName)) 					!=
									NODE_CURR_FOLDER_TAG							&&
									(*((__int32*)(data.cFileName)) & 0x00ffffff) 	!=
									NODE_PREV_FOLDER_TAG
								)
							)
							{
								unsigned int	nameLen		= strlen_cas(data.cFileName);
								char*			newParam	= HeapAlloc(procHeap, 0, inPathLen + nameLen + 3);

								strcpy_cas(newParam, inPath);
								strcpy_cas(&newParam[inPathLen - 1], data.cFileName);

								--nameLen;

								newParam[inPathLen + nameLen	] = '\\';
								newParam[inPathLen + nameLen + 1] = '*';
								newParam[inPathLen + nameLen + 2] = 0;

								LI_FULL(size) += LI_FULL(get_folder_size(newParam));

								HeapFree(procHeap, 0, newParam);
							}
					}
				else
					{
						unsigned int	nameLen		= strlen_cas(data.cFileName);
						char*			filePath	= &data.cFileName[nameLen - WAV_EXTENSION_LENGTH];

						CharLowerA(filePath);

						if(*(unsigned __int32*)(filePath) == WAV_EXTENSION)
							{
								filePath					= HeapAlloc(procHeap, 0, inPathLen + nameLen + 1);

								strcpy_cas(filePath, inPath);
								strcpy_cas(&filePath[inPathLen - 1], data.cFileName);

								waveFile = CreateFileA	(
															filePath,
															GENERIC_READ,
															0,
															NULL,
															OPEN_EXISTING,
															FILE_ATTRIBUTE_NORMAL,
															NULL
														);

								if(header = read_wav_header_simple(waveFile))
									{
										LI_FULL(size) += header->data.chunkSize;
										release_wav_header_simple(header);
									}

								CloseHandle(waveFile);

								HeapFree(procHeap, 0, filePath);
							}
					}

			__continue:
					{}
			}
		while(FindNextFileA(currFile, &data));

		FindClose(currFile);

		return size;
	}

//inPath should ends with	"\\*"
//copyPath should ends with	"\\"
void process_wav_folder_copy(char* inPath, char* copyPath, char* inReserved, double inVolume, hProgressCallback callback, LARGE_INTEGER allSize, PLARGE_INTEGER currSize)
	{
		hsWAVImage			image;
		WIN32_FIND_DATAA	data;
		HANDLE				currFile;

		unsigned int		localPathLen;
		char*				localPath;
		char				needToCreate	= 1;

		unsigned int		reservedLen		= 0;

		char*				copiedPath		= NULL;
		unsigned int		copyPathLen		= strlen_cas(copyPath);
		unsigned int		pathLen			= strlen_cas(inPath);

		if(inReserved)
			reservedLen						= strlen_cas(inReserved);
		else
			{
				copiedPath	= inPath + pathLen - 3;

				if(*copiedPath != ':')
					{
						while(*copiedPath != '\\')
							--copiedPath;

						++copiedPath;

						char* tmpPath = HeapAlloc(procHeap, 0, copyPathLen + strlen_cas(copiedPath) + 1);

						strcpy_cas(tmpPath,					copyPath	);
						
						unsigned int cpyIndex = copyPathLen;

						while(copiedPath[cpyIndex - copyPathLen] != '*')
							{
								tmpPath[cpyIndex] = copiedPath[cpyIndex - copyPathLen];
								++cpyIndex;
							}

						tmpPath[cpyIndex] = 0;

						copyPath	= tmpPath;
						copyPathLen = strlen_cas(copyPath);
					}
				else
					copiedPath = NULL;

				reservedLen = 0;
			}

		if(inReserved)
			{
				localPathLen				= pathLen + reservedLen;
				localPath					= HeapAlloc(procHeap, 0, localPathLen + 1);

				strcpy_cas(localPath, inPath);
				strcpy_cas(&localPath[pathLen], inReserved);

				currFile					= FindFirstFileA(localPath, &data);
			}
		else
			{
				localPathLen				= pathLen;
				localPath					= HeapAlloc(procHeap, 0, localPathLen + 1);
				strcpy_cas(localPath, inPath);

				currFile					= FindFirstFileA(localPath, &data);
				localPath[--localPathLen]	= 0;
			}

		do
			{
				if(data.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT	)
					goto __continue;

				else if(data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						if	(
								!*data.cFileName									||
								(
									*((__int16*)(data.cFileName)) 					!=
									NODE_CURR_FOLDER_TAG							&&
									(*((__int32*)(data.cFileName)) & 0x00ffffff) 	!=
									NODE_PREV_FOLDER_TAG
								)
							)
							{
								char*			newReserved;
								unsigned int	newLen;

								if(inReserved)
									{
										newLen				= reservedLen + strlen_cas(data.cFileName) + 2;
										newReserved			= HeapAlloc(procHeap, 0, newLen); 

										strcpy_cas(newReserved,						inReserved		);
										strcpy_cas(&newReserved[reservedLen - 1],	data.cFileName	);
									}
								else
									{
										newLen				= strlen_cas(data.cFileName) + 3;
										newReserved			= HeapAlloc(procHeap, 0, newLen); 

										strcpy_cas(newReserved, data.cFileName);
									}

								newReserved[newLen - 3] = '\\';
								newReserved[newLen - 2] = '*';
								newReserved[newLen - 1] = 0;

								if(inReserved)
									process_wav_folder_copy(inPath, copyPath, newReserved, inVolume, callback, allSize, currSize);
								else
									process_wav_folder_copy(localPath, copyPath, newReserved, inVolume, callback, allSize, currSize);

								HeapFree(procHeap, 0, newReserved);
							}
					}
				else
					{
						unsigned int	nameLen		= strlen_cas(data.cFileName);
						char*			newPath		= &data.cFileName[nameLen - WAV_EXTENSION_LENGTH];

						CharLowerA(newPath);

						if(*(unsigned __int32*)(newPath) == WAV_EXTENSION)
							{
								
								newPath				= HeapAlloc(procHeap, 0, localPathLen + nameLen + 1);

								strcpy_cas(newPath, localPath);

								if(inReserved)
									strcpy_cas(&newPath[localPathLen - 1], data.cFileName);
								else
									strcpy_cas(&newPath[localPathLen], data.cFileName);

								newPath[localPathLen + nameLen]	= 0;
						
								HANDLE			file		= CreateFileA	(
																				newPath, 
																				GENERIC_READ, 
																				0, 
																				NULL, 
																				OPEN_EXISTING, 
																				FILE_ATTRIBUTE_NORMAL, 
																				NULL
																			);

								if(image = read_wav_image_simple(file))
									{		
										CloseHandle(file);

										change_volume_of_image(image, inVolume, callback, allSize, currSize);
										/*
										currSize->QuadPart += image->header->riff.identifiers.chunkSize + sizeof(sChunkIdentifiers);

										callback((double)(currSize->QuadPart) / (double)(LI_FULL(allSize)));
										*/

										unsigned int	clonePathLen	= copyPathLen + reservedLen + nameLen;
										char*			clonePath		= HeapAlloc(procHeap, 0, clonePathLen + 1);

										strcpy_cas(clonePath, copyPath);

										if(inReserved)
											strcpy_cas(&clonePath[copyPathLen], inReserved);

										//***		Check if folder exist and create new file in copyPath

										if(needToCreate)
											{
												wchar_t* wPath = HeapAlloc(procHeap, 0, (strlen_cas(clonePath) << 1) + 2);
												mbs_to_wcs_cas(clonePath, wPath);
												
												create_directories(wPath);

												HeapFree(procHeap, 0, wPath);

												needToCreate = 0;
											}

										//***

										if(inReserved)
											strcpy_cas(&clonePath[copyPathLen + reservedLen - 1], data.cFileName);
										else
											strcpy_cas(&clonePath[copyPathLen + reservedLen], data.cFileName);

										file		= CreateFileA	(
																		clonePath, 
																		GENERIC_WRITE, 
																		0, 
																		NULL, 
																		CREATE_ALWAYS, 
																		FILE_ATTRIBUTE_NORMAL,
																		NULL
																	); 

										write_wav_image_simple	(file, image);

										release_wav_image_simple(image		);
										HeapFree(procHeap, 0, clonePath		);
									}

								CloseHandle(file);

								HeapFree(procHeap, 0, newPath);
							}
					}

			__continue:
				{}
			}
		while(FindNextFileA(currFile, &data));

		FindClose(currFile);

		HeapFree(procHeap, 0, localPath);

		if(copiedPath)
			HeapFree(procHeap, 0, copyPath);
	}

//Path should be with filter *
void process_wav_folder_replace(char* inPath, double inVolume, hProgressCallback callback, LARGE_INTEGER allSize, PLARGE_INTEGER currSize)
	{
		hsWAVImage			image;
		WIN32_FIND_DATAA	data;
		HANDLE				currFile = FindFirstFileA(inPath, &data);

		do
			{
				if(data.dwFileAttributes & FILE_ATTRIBUTE_REPARSE_POINT)
					goto __continue;

				else if(data.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY)
					{
						if	(
								!*data.cFileName									||
								(
									*((__int16*)(data.cFileName)) 					!=
									NODE_CURR_FOLDER_TAG							&&
									(*((__int32*)(data.cFileName)) & 0x00ffffff) 	!=
									NODE_PREV_FOLDER_TAG
								)
							)
							{
								unsigned int	pathLen	= strlen_cas(inPath) - 1;
								unsigned int	nameLen	= strlen_cas(data.cFileName);

								char*			newPath	= HeapAlloc(procHeap, 0, pathLen + nameLen + 3);
								strcpy_cas(newPath, inPath);
								strcpy_cas(&newPath[pathLen], data.cFileName);

								newPath[pathLen + nameLen		] = '\\';
								newPath[pathLen + nameLen + 1	] = '*';
								newPath[pathLen + nameLen + 2	] = 0;

								process_wav_folder_replace(newPath, inVolume, callback, allSize, currSize);

								HeapFree(procHeap, 0, newPath);
							}
					}
				else
					{
						unsigned int	nameLen		= strlen_cas(data.cFileName);
						char*			newPath		= &data.cFileName[nameLen - WAV_EXTENSION_LENGTH];

						CharLowerA(newPath);

						if(*(unsigned __int32*)(newPath) == WAV_EXTENSION)
							{
								unsigned int	pathLen		= strlen_cas(inPath) - 1;

								newPath		= HeapAlloc(procHeap, 0, pathLen + nameLen + 1);
								strcpy_cas(newPath, inPath);
								strcpy_cas(&newPath[pathLen], data.cFileName);

								newPath[pathLen + nameLen]	= 0;
						
								HANDLE			file		= CreateFileA	(
																				newPath, 
																				GENERIC_READ | GENERIC_WRITE, 
																				0, 
																				NULL, 
																				OPEN_EXISTING, 
																				FILE_ATTRIBUTE_NORMAL, 
																				NULL
																			);

								if(image = read_wav_image_simple(file))
									{
										change_volume_of_image(image, inVolume, callback, allSize, currSize);

										/*
										currSize->QuadPart += image->header->riff.identifiers.chunkSize + sizeof(sChunkIdentifiers);

										callback((double)(currSize->QuadPart) / (double)(LI_FULL(allSize)));
										*/

										SetFilePointer(file, 0, 0, FILE_BEGIN);

										write_wav_image_simple(file, image);

										release_wav_image_simple(image);
									}

								CloseHandle(file);
							}
					}

			__continue:
				{}
			}
		while(FindNextFileA(currFile, &data));
	}

unsigned __int16 change_volume_of_image(hsWAVImage inImage, double inCoef, hProgressCallback callback, LARGE_INTEGER allSize, PLARGE_INTEGER currSize)
	{
		void* curr = inImage->data;

		switch(inImage->header->chunkFmt.fmt.dataPart.audioFormat)
			{
				case WAVE_FORMAT_PCM:
					{
						switch(inImage->header->chunkFmt.fmt.dataPart.bitsPerSample)
							{
								case BIT_RATE_8:
									{
										unsigned __int64	sampleCount = inImage->header->data.chunkSize;
										__int16				value;

										while(sampleCount)
											{
												value = (__int16)(*((__int8*)(curr)) * inCoef) & 0x00ff;

												*((__int8*)(curr)) = (__int8)(value);

												++((__int8*)(curr));
												--sampleCount;

												currSize->QuadPart += sizeof(__int8);

												callback((double)(currSize->QuadPart) / (double)(LI_FULL(allSize)));
											}

										goto __ret;
									}
								case BIT_RATE_16:
									{
										unsigned __int64	sampleCount = inImage->header->data.chunkSize >> 1;
										__int32				value;

										while(sampleCount)
											{
												value = (__int32)(*((__int16*)(curr)) * inCoef) & 0x0000ffff;

												*((__int16*)(curr)) = value;

												++((__int16*)(curr));
												--sampleCount;

												currSize->QuadPart += sizeof(__int16);

												callback((double)(currSize->QuadPart) / (double)(LI_FULL(allSize)));
											}

										goto __ret;
									}
								case BIT_RATE_24:
									{
										unsigned __int64	sampleCount = inImage->header->data.chunkSize / 3;
										__int32				value;

										while(sampleCount)
											{
												//getting first 3 informative bytes (24 bits)
												value = *((__int32*)(curr)) & 0x00ffffff;

												//if it was negative value need to make
												//buffer value negative too
												if(*((__int32*)(curr)) & 0x00800000)
													value |= 0xff000000;

												value = (__int32)(value * inCoef) & 0x00ffffff;

												/*
													Optimize writing with one machine word at time.
													It could be done like this:

													*HANDLE_OFFSET(curr, 0, __int8) = *HANDLE_OFFSET(&value, 0, __int8);
													*HANDLE_OFFSET(curr, 1, __int8) = *HANDLE_OFFSET(&value, 1, __int8);
												*/
												*HANDLE_OFFSET(curr, 0, __int16	) = *HANDLE_OFFSET(&value, 0, __int16	);
												*HANDLE_OFFSET(curr, 2, __int8	) = *HANDLE_OFFSET(&value, 2, __int8	);

												curr = HANDLE_OFFSET(curr, 3, void);
												--sampleCount;

												currSize->QuadPart += 3;

												callback((double)(currSize->QuadPart) / (double)(LI_FULL(allSize)));
											}

										goto __ret;
									}
								case BIT_RATE_32:
									{
										unsigned __int64	sampleCount = inImage->header->data.chunkSize >> 2;
										__int64				value;

										while(sampleCount)
											{
												value = (__int64)(*((__int32*)(curr)) * inCoef) & 0x00000000ffffffff;

												*((__int32*)(curr)) = (__int32)(value);

												++((__int32*)(curr));
												--sampleCount;

												currSize->QuadPart += sizeof(__int32);

												callback((double)(currSize->QuadPart) / (double)(LI_FULL(allSize)));
											}

										goto __ret;
									}
								default:
									return WAVE_FORMAT_UNKNOWN;
							}
					}
				case WAVE_FORMAT_IEEE_FLOAT:
					{
						unsigned __int64	sampleCount = inImage->header->data.chunkSize >> 2;
						double				value;

						while(sampleCount)
							{
								value = (*((float*)(curr)) * inCoef);

								if(value > TOP_32_BIT_FLOAT)
									value = TOP_32_BIT_FLOAT;

								*((float*)(curr)) = (float)(value);

								++((float*)(curr));
								--sampleCount;

								currSize->QuadPart += sizeof(float);

								callback((double)(currSize->QuadPart) / (double)(LI_FULL(allSize)));
							}

						goto __ret;
					}
				default:
					return WAVE_FORMAT_UNKNOWN;
			}

	__ret:

		return inImage->header->chunkFmt.fmt.dataPart.audioFormat;
	}

//***		image

hsWAVImage read_wav_image_simple(HANDLE inFile)
	{
		hsWAVImage	image;
		hsWAVHeader header;
		DWORD		read;

		if(header = read_wav_header_simple(inFile))
			{
				image			= HeapAlloc(procHeap, 0, sizeof(sWAVImage));

				image->header	= header;

				image->data		= HeapAlloc(procHeap, 0, image->header->data.chunkSize);
				ReadFile(inFile, image->data, image->header->data.chunkSize, &read, NULL);

				if(LI_FULL(image->header->postExtraSize))
					{
						image->postExtraData = HeapAlloc(procHeap, 0, (SIZE_T)LI_FULL(image->header->postExtraSize));
						ReadFile(inFile, image->postExtraData, (DWORD)LI_FULL(image->header->postExtraSize), &read, NULL);
					}
				else
					image->postExtraData = NULL;

				return image;
			}

		return NULL;
	}

void write_wav_image_simple(HANDLE inFile, hsWAVImage inImage)
	{
		write_wav_header_simple(inFile, inImage->header);

		DWORD written;

		if(inImage->header->data.chunkSize)
			WriteFile(inFile, inImage->data, inImage->header->data.chunkSize, &written, NULL);

		if(LI_FULL(inImage->header->postExtraSize))
			WriteFile(inFile, inImage->postExtraData, (DWORD)LI_FULL(inImage->header->postExtraSize), &written, NULL);
	}

void release_wav_image_simple(hsWAVImage inImage)
	{
		release_wav_header_simple(inImage->header);

		if(inImage->header->data.chunkSize && inImage->data)
			HeapFree(procHeap, 0, inImage->data);

		if(LI_FULL(inImage->header->postExtraSize) && inImage->postExtraData)
			HeapFree(procHeap, 0, inImage->postExtraData);
	}
	
//***		header

void release_wav_header_simple(hsWAVHeader inHeader)
	{
		hsEXTRASubChunk curr;

		if(inHeader->postRiff.subchunksCount && inHeader->postRiff.subchunksSize)
			{
				while(inHeader->postRiff.extraSubchunks)
					{
						curr								= inHeader->postRiff.extraSubchunks;
						inHeader->postRiff.extraSubchunks	= curr->nextChunk;

						HeapFree(procHeap, 0, curr->data.subchunkData	);
						HeapFree(procHeap, 0, curr						);
					}
			}

		if(inHeader->chunkFmt.fmtExtra.extraParamSize)
			HeapFree(procHeap, 0, inHeader->chunkFmt.fmtExtra.extraParams);

		if(inHeader->postFmt.subchunksCount && inHeader->postFmt.subchunksSize)
			{
				while(inHeader->postFmt.extraSubchunks)
					{
						curr								= inHeader->postFmt.extraSubchunks;
						inHeader->postFmt.extraSubchunks	= curr->nextChunk;

						HeapFree(procHeap, 0, curr->data.subchunkData	);
						HeapFree(procHeap, 0, curr						);
					}
			}

		HeapFree(procHeap, 0, inHeader);
	}

void write_wav_header_simple(HANDLE inFile, hsWAVHeader inHeader)
	{
		DWORD written;
		hsEXTRASubChunk curr;

		WriteFile(inFile, &inHeader->chunkRiff.riff, sizeof(sRIFFHeader), &written, NULL); 

		if(inHeader->postRiff.subchunksCount && inHeader->postRiff.subchunksSize)
			{
				curr = inHeader->postRiff.extraSubchunks;

				while(curr)
					{
						WriteFile(inFile, &curr->data.identifiers, sizeof(sChunkIdentifiers),			&written, NULL);
						WriteFile(inFile, curr->data.subchunkData, curr->data.identifiers.chunkSize,	&written, NULL);

						curr = curr->nextChunk;
					}
			}

		WriteFile(inFile, &inHeader->chunkFmt.fmt,	sizeof(sFMTHeader),	&written, NULL);

		if(inHeader->chunkFmt.fmtExtra.extraParamSize)
			WriteFile(inFile, inHeader->chunkFmt.fmtExtra.extraParams, inHeader->chunkFmt.fmtExtra.extraParamSize,	&written, NULL);

		if(inHeader->postFmt.subchunksCount && inHeader->postFmt.subchunksSize)
			{
				curr = inHeader->postFmt.extraSubchunks;

				while(curr)
					{
						WriteFile(inFile, &curr->data.identifiers, sizeof(sChunkIdentifiers),			&written, NULL);
						WriteFile(inFile, curr->data.subchunkData, curr->data.identifiers.chunkSize,	&written, NULL);

						curr = curr->nextChunk;
					}
			}

		WriteFile(inFile, &inHeader->data, sizeof(sChunkIdentifiers), &written, NULL);
	}

hsWAVHeader read_wav_header_simple(HANDLE inFile)
	{
		LARGE_INTEGER fileSize;

		GetFileSizeEx(inFile, &fileSize);

		if(LI_FULL(fileSize) < sizeof(sRIFFHeader))
			return NULL;

		DWORD			read;
		DWORD			allReadBytes	= 0;
		hsWAVHeader		header			= HeapAlloc(procHeap, 0, sizeof(sWAVHeader));
		hsEXTRASubChunk curr			= NULL;

		ReadFile(inFile, &header->chunkRiff.riff, sizeof(sRIFFHeader), &read, NULL);
		allReadBytes += read;

		if	(
				*(unsigned __int32*	)(&header->chunkRiff.riff.identifiers.chunkId	)	!= RIFF_SECTION	||
				*(unsigned __int32*	)(&header->chunkRiff.riff.format				)	!= FORMAT_WAVE
			)
			{
				HeapFree(procHeap, 0, header);
				return NULL;
			}

		header->postRiff.subchunksCount = 0;
		header->postRiff.subchunksSize	= 0;
		header->postRiff.extraSubchunks = NULL;

	__postRiffChunks:

		ReadFile(inFile, &header->data, sizeof(sChunkIdentifiers), &read, NULL);

		if(read)
			{
				allReadBytes += read;

				if(*(unsigned __int32*)(&header->data.chunkId) != FMT_SECTION)
					{
						if(curr)
							{
								curr->nextChunk							= HeapAlloc(procHeap, 0, sizeof(sEXTRASubChunk));
								curr									= curr->nextChunk;
							}
						else
							{
								curr									= HeapAlloc(procHeap, 0, sizeof(sEXTRASubChunk));
								header->postRiff.extraSubchunks			= curr;
							}

						header->postRiff.subchunksSize					+= header->data.chunkSize;
						++header->postRiff.subchunksCount;

						curr->nextChunk									= NULL;
						*(__int32*)(&curr->data.identifiers.chunkId)	= *(__int32*)(&header->data.chunkId);
						curr->data.identifiers.chunkSize				= header->data.chunkSize;

						curr->data.subchunkData							= HeapAlloc(procHeap, 0, curr->data.identifiers.chunkSize);
						ReadFile(inFile, curr->data.subchunkData, curr->data.identifiers.chunkSize, &read, NULL);
						allReadBytes += read;

						goto __postRiffChunks;
					}

				goto __fmt;
			}

		while(header->postRiff.extraSubchunks)
			{
				curr							= header->postRiff.extraSubchunks;
				header->postRiff.extraSubchunks = curr->nextChunk;

				HeapFree(procHeap, 0, curr->data.subchunkData);
				HeapFree(procHeap, 0, curr);
			}

		HeapFree(procHeap, 0, header);

		return NULL;
	
	__fmt:
		
		//We got fmt identifiers in data now
		//Lets move it to fmt
		*(__int32*)(&header->chunkFmt.fmt.identifiers.chunkId)	= *(__int32*)(&header->data.chunkId);
		header->chunkFmt.fmt.identifiers.chunkSize				= header->data.chunkSize;

		ReadFile(inFile, &header->chunkFmt.fmt.dataPart, sizeof(sFMTDataPart), &read, NULL);
		allReadBytes += read;

		header->chunkFmt.fmtExtra.extraParamSize				= 0;
		header->chunkFmt.fmtExtra.extraParams					= NULL;

		if(header->chunkFmt.fmt.identifiers.chunkSize > sizeof(sFMTDataPart))
			{
				header->chunkFmt.fmtExtra.extraParamSize	= header->chunkFmt.fmt.identifiers.chunkSize - sizeof(sFMTDataPart	);

				header->chunkFmt.fmtExtra.extraParams		= HeapAlloc(procHeap, 0, header->chunkFmt.fmtExtra.extraParamSize	);
				ReadFile(inFile, header->chunkFmt.fmtExtra.extraParams, header->chunkFmt.fmtExtra.extraParamSize, &read, NULL	);
				allReadBytes += read;
			}

		*(__int32*)(&header->data.chunkId)		= 0;

		header->postFmt.subchunksSize			= 0;
		header->postFmt.subchunksCount			= 0;
		header->postFmt.extraSubchunks			= NULL;

		curr									= NULL;

		while(read)
			{
				ReadFile(inFile, &header->data, sizeof(sChunkIdentifiers), &read, NULL);

				if(!read)
					goto __free_and_exit;

				allReadBytes += read;

				if(*(unsigned int*)(&header->data.chunkId) == DATA_SECTION)
					{
						LI_FULL(fileSize) -= allReadBytes;

						if(LI_FULL(fileSize) >= header->data.chunkSize)
							{
								LI_FULL(header->postExtraSize) = LI_FULL(fileSize) - header->data.chunkSize;

								return header;
							}

						goto __free_and_exit;
					}

				if(curr)
					{
						curr->nextChunk							= HeapAlloc(procHeap, 0, sizeof(sEXTRASubChunk));
						curr									= curr->nextChunk;
					}
				else
					{
						curr									= HeapAlloc(procHeap, 0, sizeof(sEXTRASubChunk));
						header->postFmt.extraSubchunks			= curr;
					}

				header->postFmt.subchunksSize					+= header->data.chunkSize;
				++header->postFmt.subchunksCount;

				curr->nextChunk									= NULL;
				*(__int32*)(&curr->data.identifiers.chunkId)	= *(__int32*)(&header->data.chunkId);
				curr->data.identifiers.chunkSize				= header->data.chunkSize;

				curr->data.subchunkData							= HeapAlloc(procHeap, 0, curr->data.identifiers.chunkSize);
				ReadFile(inFile, curr->data.subchunkData, curr->data.identifiers.chunkSize, &read, NULL);
				allReadBytes += read;
			}

	__free_and_exit:

		if(header->chunkFmt.fmtExtra.extraParams)
			HeapFree(procHeap, 0, header->chunkFmt.fmtExtra.extraParams);

		while(header->postFmt.extraSubchunks)
			{
				curr							= header->postFmt.extraSubchunks->nextChunk;

				HeapFree(procHeap, 0, header->postFmt.extraSubchunks->data.subchunkData);
				HeapFree(procHeap, 0, header->postFmt.extraSubchunks);

				header->postFmt.extraSubchunks	= curr;
			}

		HeapFree(procHeap, 0, header);

		return NULL;
	}

void create_directories(wchar_t* nodePath)
	{
		wchar_t* clone	= HeapAlloc(procHeap, 0, wcs_byte_len_cas(nodePath) + 10);
		wchar_t* base	= &clone[4];

		wcscpy_cas(clone,		L"\\\\?\\"	);
		wcscpy_cas(&clone[4],	nodePath	);

		while(*base && *base != L'\\')
			++base;

		++base;

	__loop:

		while(*base && *base != L'\\')
			++base;

		if(!*base)
			{
				HeapFree(procHeap, 0, clone);
				return;
			}

		*base = 0;

		if(GetFileAttributesW(clone) == INVALID_FILE_ATTRIBUTES)
			CreateDirectoryW(clone, NULL);

		*base = L'\\';
		++base;

		goto __loop;
	}